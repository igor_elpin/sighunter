#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <cstring>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <unistd.h>
#include "AhoCorasickPlus.h"

using namespace std;

// Signature structure
struct signature
{
    string hex;
    string desc;
    unsigned int count;

    signature()
    {
	count = static_cast<int>(0);
    }
};

// Define global parameters
string version = "0.79";
const int signature_capacity = 1000;
int sig_array_size = 0;
signature signature_array[signature_capacity] = {};

int str2int(string str)
{
    int integer;
    istringstream ss(str);
    ss >> integer;
    return integer;
}

string LoadHexFromFile(ifstream& FILE_IN)
{
    char symbol;
    string bf; //  need some buffer
    ostringstream str_buf(bf);
    streambuf* x = cout.rdbuf(str_buf.rdbuf()); // Redirect sdtout in buffer
    string hexstring;
    int count = 0;
    while (1) {
	FILE_IN.read(&symbol, 1);
	if (FILE_IN.eof())
	    break;
	// no text in sdtout !!!!  all in streambuf !!!!
	cout << setw(2) << setfill('0') << hex << static_cast<unsigned long int>(symbol & 0xff);
	count++;
    }
    // get streambuf in string )
    hexstring = str_buf.str();
    // Cancel  stdout redirect  )
    cout.rdbuf(x);
    cout << std::dec;
    if (count > 0) {
	return hexstring;
    } else {
	cout << "Input file too small... 0 bytes ? " << endl;
	exit(-1);
    }
}

void LoadBase(ifstream& FILE_BASE, AhoCorasickPlus& tree)
{
    string hexline;
    string segment;
    int count = 0;
    while (1) {
	// if (FILE_BASE.eof()) {

	//	break;
	//}
	std::vector<std::string> seglist;
	// read signature from file
	getline(FILE_BASE, hexline);
	std::stringstream sigstring(hexline);
	//  Parse signature format
	while (std::getline(sigstring, segment, ';')) {
	    seglist.push_back(segment);
	}
	if (hexline.compare("") == 0) {
	    sig_array_size = count;
	    break;
	}
	AhoCorasickPlus::EnumReturnStatus status;
	AhoCorasickPlus::PatternId patId = count;
	// fill signature structure
	signature_array[count].hex = seglist.at(0);
	signature_array[count].desc = seglist.at(1);
	status = tree.addPattern(seglist.at(0), patId);
	if (status != AhoCorasickPlus::RETURNSTATUS_SUCCESS) {
	    std::cout << "Failed to load signature :: " << signature_array[count].desc << std::endl;
	    exit(30);
	}
	count++;
    }
}

int main(int argc, char** argv)
{
    cout << "\033[1;36m  SigHunter  v. " << version
         << " 2015-2016 (c) Igor Elpin \033[0m  <igoryelpin@gmail.com>" << endl;
    if (argc < 4) {
	cout << "Usage: \"sighunter <signature database>  <file to scan>  "
	     << " < report file > \"" << endl;
	return 1;
    }
    ifstream FILE_IN(argv[2], ios::in | ios::binary);
    ifstream FILE_BASE(argv[1], ios::in);
    ofstream FILE_OUT(argv[3], ios::out);
    if (!FILE_IN.is_open()) {
	cout << "\033[0m \033[1;31m Can not open input file \" \033[0m" << argv[2] << '\"' << endl;
	return 2;
    }
    if (!FILE_BASE.is_open()) {
	cout << "Can not open signature database file \"" << argv[1] << '\"' << endl;
	return 2;
    }
    AhoCorasickPlus tree;
    //  Read signatures base into memory (bohr)
    LoadBase(FILE_BASE, tree);
    //  Read all file contents in string buffer
    string content = LoadHexFromFile(FILE_IN);
    //  Bohr initiliazition
    tree.finalize();
    AhoCorasickPlus::Match aMatch;
    //  Start signature check
    tree.search(content, true);

    int times = 0;
    while (tree.findNext(aMatch)) {
	times++;
	if (times > 0) {
	    // increment matched signatures.
	    signature_array[aMatch.id].count++;
	}
    }

    if (times <= 1) {
	FILE_OUT << "InfoWatch SigHunter:: "
	         << "Файл  не содержит других файловых сигнатур\n";
    } else {
	FILE_OUT << "InfoWatch SigHunter:: "
	         << "Файл содержит несколько файловых сигнатур" << endl;
    }
    int sig_summ = 0;
    // count overall occurences of signatures
    for (int iterator = 0; iterator < sig_array_size; iterator++) {
	if (signature_array[iterator].count > 0) {
	    sig_summ = sig_summ + signature_array[iterator].count;
	}
    }
    // list matched signatures
    FILE_OUT << "Сигнатурная карта файловых заголовков: " << endl;
    FILE_OUT << "Число совпадений   "
             << "   Процент сходства           "
             << "    Формат файла     " << endl;
    for (int iterator = 0; iterator < sig_array_size; iterator++) {
	if (signature_array[iterator].count > 0) {
	    double percentage = (signature_array[iterator].count) * 100 / sig_summ;
	    FILE_OUT
	    << signature_array[iterator].count << " раз(а)             " << percentage << " %                    "
	    << signature_array[iterator].desc << "         " << endl;
	}
    }
    return 0;
}